function y = hs(t)

    y = exp(-(1/2).*((t-13)./4.5).^4);

end