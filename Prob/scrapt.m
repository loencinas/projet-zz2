% Remarque :
% 1) Les activités ne durent pas assez longtemps et s'enchainent : une
% vache peut aller manger, puis se promener, puis retourner manger.
% 2) Peut-être changer la manière dont les cumuls évoluent
% 3) Les vaches ne retournent pas dormir le soir, et elles ne vont pas
% digérer après avoir manger.
% 4) On peut régler l'importance des règles pour une même activité, mais on
% ne peut pas régler l'importance des règles entre activité.


T = 1:24;
L = length(T);
n = 0.01;

nf = 0.4; ne = 0.4; ns = 0.4;
cf = 4; ce = 4; cs = 4;
Mf = 16; Me = 16; Ms = 16;
a1 = 1; a2 = 1; a3 = 1;
min_a = -(a1+a2)/2; max_a = (a1+a2+a3)/2;
c1 = 1; c2 = 1; c3 = 0; c4 = 1;
min_c = -(c1+c2)/2; max_c = (c1+c2+c3+c4)/2;
b1 = 1; b2 = 1; b3 = 1; b4 = 1; b5 = 1;
min_b = -(b1+b2+b3+b4)/2; max_b = (b1+b2+b3+b4+b5)/2;
M = zeros(1,length(T));
P = zeros(1,length(T));
D = zeros(1,length(T)); D(1) = 1; D(2) = 1;

PM = 0; PP = 0; PD = 0;

for i=2:L-1

    delta_f = a1*(cf/Mf - 0.5) + a2*(hr(T(i+1)) - 0.5) + a3/2*M(i);
    delta_f = normalisation(delta_f,min_a,max_a);
    nf = max(0,min(nf+n*delta_f,1));

    delta_e = c1*(cs/Ms - 0.5) + c2*M(i)*(cf/Mf - 0.5) + c3*g(cf/Mf) + c4/2*P(i);
    delta_e = normalisation(delta_e,min_c,max_c);
    ne = max(0,min(ne+n*delta_e,1));

    delta_s = b1*(ce/Me - 0.5) + b2*P(i)*(cf/Mf - 0.5) + b3*(cs/Ms - 0.5) + b4*(hs(T(i+1)) - 0.5) + b5/2*D(i);
    delta_s = normalisation(delta_s,min_b,max_b);
    ns = max(0,min(ns+n*delta_s,1));

%     sum = nf + ne + ns;
%     nf = normalisation(nf,0,sum);
%     ne = normalisation(ne,0,sum);
%     ns = normalisation(ns,0,sum);
    % Pas obligatoire

    PM = nf/(nf+ne+ns); PP = ne/(nf+ne+ns); PD = ns/(nf+ne+ns);

    V = [PM*ones(1,round(PM*10000)) PP*ones(1,round(PP*10000)) PD*ones(1,round(PD*10000))];
    nw = V(randi(length(V)));

    switch nw
        case PM
            if D(i) == 1
                P(i+1) = 1;
            else
                M(i+1) = 1;
            end
            cf = max(0,min(cf+1,Mf));
            ce = max(0,min(ce-1,Me));
            cs = max(0,min(cs-1,Ms));

%             cf = max(0,min(cf-1,Mf));
%             ce = max(0,min(ce+1,Me));
%             cs = max(0,min(cs+1,Ms));
        case PP
            P(i+1) = 1;
            cf = max(0,min(cf-1,Mf));
            ce = max(0,min(ce+1,Me));

%             cf = max(0,min(cf+1,Mf));
%             ce = max(0,min(ce-1,Me));
        case PD
            if M(i) == 1
                P(i+1) = 1;
            else
                D(i+1) = 1;
            end
            cf = max(0,min(cf-1,Mf));
            ce = max(0,min(ce-1,Me));
            cs = max(0,min(cs+1,Ms));

%             cf = max(0,min(cf+1,Mf));
%             ce = max(0,min(ce+1,Me));
%             cs = max(0,min(cs-1,Ms));
    end
end

figure;
stairs(T,M)
title('Manger');
axis([1 L+1 -0.1 1.1]);
grid on;
figure;
stairs(T,P);
title('Promener');
axis([1 L+1 -0.1 1.1]);
grid on;
figure;
stairs(T,D);
title('Dormir');
axis([1 L+1 -0.1 1.1]);
grid on;

figure;
x = 0:1/100:24;
y = hr(x);
plot(x,y);
grid on;

TOT = M + P + D;