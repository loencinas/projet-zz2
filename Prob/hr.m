% On part du principe que t est une heure sur la journée.
% Une vache passe 6 à 8h de sa journée à manger, cette fonction est doonc
% assez dur à déterminer.
% 
% 1er idée : comme les vaches.
% On va considérer que le fermier mets de la nourriture en quantité
% suffisante le matin vers les 6h. Donc que hr(6) = 1.

function y = hr(t)
    y = exp(-(1/2).*((t-7)./0.5).^2) + exp(-(1/2).*((t-13)./0.5).^2) + exp(-(1/2).*((t-20)./0.5).^2);
end