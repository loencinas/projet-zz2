function y = normalisation(x,min,max)
    y = (2*x-max-min)/(max-min);
end