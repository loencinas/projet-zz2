function y = g(x)
    if(x>=0 && x<=1/2)
        y = 2*x;
    end
    if(x>1/2 && x<=1)
        y = -2*(x-1);
    end
    if(x<0 && x>1)
        y = 0;
    end
end