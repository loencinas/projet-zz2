T = 1:24;

M = zeros(1,length(T));
P = zeros(1,length(T));
D = zeros(1,length(T));
for i=1:10
    [Mbis,Pbis,Dbis] = vache(T);
    M = M + Mbis;
    P = P + Pbis;
    D = D + Dbis;
end

figure;
stairs(T,M)
title('Manger');
grid on;
figure;
stairs(T,P);
title('Promener');
grid on;
figure;
stairs(T,D);
title('Dormir');
grid on;

