T = readtable('dataset1_filtered.csv');
nb = 28; % nombre de vache

% idV : liste des identifiants des différentes vaches
% nbV : nombre d'heures enregistrés pour chaque vache
% indV : indice de la première heure enregistrée pour chaque vache dans M (ou T)
[idV,nbV,indV] = info_table(T,nb);

% extension() à adapter
% courbe_proba() à adapter

[T_nettoye,indV_nettoye,nbV_nettoye] = nettoyage_date(T,nb,indV);
T_nn = nettoyage_valeur(T_nettoye);

prob_nn = courbe_proba(T_nn,1);

