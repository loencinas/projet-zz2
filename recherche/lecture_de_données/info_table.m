function [idV,nbV,indV] = info_table(T,nb)
    idV = T{1,1}; % en utilisant {}, T{} renvoie un array et pas un table
    nbV = zeros(1,nb);
    id = T{1,1};
    ind = 1;
    for i=1:height(T)
        nbV(ind) = nbV(ind) + 1;
        if T{i,1} ~= id
            id = T{i,1};
            idV = [idV T{i,1}];
            ind = ind + 1;
        end
    end
    
    indV = zeros(1,nb);
    indV(1) = 1;
    for i=2:nb
        indV(i) = sum(nbV(1:i-1));
    end
end