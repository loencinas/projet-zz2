function [idV,nbV,indV] = info(M,nb)
    idV = M(1,1);
    nbV = zeros(1,nb);
    id = M(1,1);
    ind = 1;
    for i=1:length(M)
        nbV(ind) = nbV(ind) + 1;
        if M(i,1) ~= id
            id = M(i,1);
            idV = [idV M(i,1)];
            ind = ind + 1;
        end
    end
    
    indV = zeros(1,nb);
    indV(1) = 1;
    for i=2:nb
        indV(i) = sum(nbV(1:i-1));
    end
end