function prob = courbe_proba(T,nb_jour)

    h = height(T);
    nb_heure = 24*nb_jour;
    prob = zeros(nb_heure,3); % In alley, rest, eat
    somme = zeros(nb_heure,1);
    
    for i=1:h
        prob(T.hour(i)+1,1) = prob(T.hour(i)+1,1) + T.IN_ALLEYS(i);
        prob(T.hour(i)+1,2) = prob(T.hour(i)+1,2) + T.REST(i);
        prob(T.hour(i)+1,3) = prob(T.hour(i)+1,3) + T.EAT(i);

        somme(T.hour(i)+1) = somme(T.hour(i)+1) + sum(T{i,4:6});
    end
    
    prob = prob ./ somme;
    prob(:,2) = prob(:,2) + prob(:,1);
    prob(:,3) = prob(:,3) + prob(:,2);
    prob = prob';
    
    figure;
    area(0:nb_heure-1,prob(3,:));
    hold on;
    area(0:nb_heure-1,prob(2,:));
    hold on;
    area(0:nb_heure-1,prob(1,:));
    legend('Eat','Rest','In Alley'); % Eat en premier, puis In alley et sleep.
end