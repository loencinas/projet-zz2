function [T,indV,nbV] = nettoyage_date(v_T,nb,v_indV)

    % l'indexiation logique permet d'avoir les valeurs en certains indices
    % d'un array en indiquant par 0 ou 1 dans un tableau si on veut
    % l'élément.
    % https://fr.mathworks.com/matlabcentral/answers/129-how-does-logical-indexing-work
    % La fonction find existe aussi mais moins performante.

    T = v_T; indV = v_indV;
    max_date_global = max(T.date);
    max_date = max(T.date(indV));
    ind = 0;
    compteur = 0;
    while max_date < max_date_global
        while ind ~= 1
            supp_log = (T.date(indV) ~= max_date);
            supp_ind = indV(supp_log);
            T(supp_ind,:) = [];
            for i=2:nb
                indV(i) = indV(i) - sum(supp_log(1:i-1));
            end
            max_date = max(T.date(indV));
            if all(supp_log == 0)
                ind = ind + 1;
            end
        end
        ind = 0;
        indV = indV+1;
        compteur = compteur + 1;
        max_date = max(T.date(indV));
    end
    indV = indV - compteur;
    len = indV(2) - indV(1) - 1;
    nbV = len*ones(1,nb);
end