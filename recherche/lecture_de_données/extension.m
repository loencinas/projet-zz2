function M = extension(Mh,idV)
    M = Mh;
    for i=1:length(idV)
        ind = 1;
        supp = 0;
        while(M(ind,1) == idV(i))
            M(ind,2) = M(ind,2) + supp*24;
            ind = ind + 1;
            if(M(ind,2) == 0)
                supp = supp + 1;
            end
        end    
    end
end