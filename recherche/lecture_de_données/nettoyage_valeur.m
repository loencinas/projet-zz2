function T = nettoyage_valeur(v_T)
    T = v_T;
    h = height(T);
    supp_ind = 1:h;
    for i=1:h
        supp_log(i) = (T.IN_ALLEYS(i) == 0 || T.EAT(i) == 0 || T.REST(i) == 0);
    end
    supp_ind = supp_ind(supp_log);
    T(supp_ind,:) = [];
end